import os
from flask import render_template, url_for, Blueprint, request, json
from werkzeug.exceptions import HTTPException, BadRequest, BadGateway, ServiceUnavailable,RequestTimeout

# create a blueprint
api = Blueprint('api', __name__)





# homepage route
@api.route('/', methods=['POST','GET'])
def index():
    return render_template('index.html')

# all other routes

