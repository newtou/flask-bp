import os
from flask import Flask
from flask_cors import CORS
from flask_fontawesome import FontAwesome
from flask_assets import Bundle, Environment

# init modules
fa = FontAwesome()
cors = CORS()

# define the assets
assets = Environment()


# define the create_app function
def create_app():
    # initialise the app instance
    
    app = Flask(__name__)

    # any app configs to come here
    app.config['SECRET_KEY'] = 'sd34rrf4rcec@e35431:xwq3;2'

    # initialise the modules with the app
    fa.init_app(app)
    cors.init_app(app)
    assets.init_app(app)

    # css js bundles
    css = Bundle('css/root.css','css/base.css','css/index.css', output='gen/main.css')
    js = Bundle('js/index.js', output='gen/main.js')

    # register the bundles
    assets.register('css_all', css)
    assets.register('js_all', js)

    # import any blueprints
    from .views import api
    from .errorhandlers import err

    # register the blueprints
    app.register_blueprint(err)
    app.register_blueprint(api)

    # return the app
    return app