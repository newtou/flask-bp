import os
from flask import Blueprint
from werkzeug.exceptions import BadGateway, BadRequest, RequestTimeout,ServiceUnavailable

# create errorblueprints
err = Blueprint('err',__name__)

# error handlers endpoints

@err.errorhandler(BadRequest)
def handle_bad_request(e):
    # return bad request page later
    return 'bad request', 400

@err.errorhandler(BadGateway)
def handle_bad_gatewat(e):
    # return bad gateway page later
    return 'bad gateway',502 

@err.errorhandler(ServiceUnavailable)
def handle_service_unavailable(e):
    # return service unavailable page
    return 'service not available', 503

@err.errorhandler(RequestTimeout)
def handle_request_timeout(e):
    # return request timeout page
    return 'request timeout', 408