# install all requirements again
pipenv install

# regenerate the requirements files
pipenv run pipenv_to_requirements

# export the app
export FLASK_APP=app

# export the environment
export FLASK_ENV=development

# run the app
flask run --host=0.0.0.0 --port=5000