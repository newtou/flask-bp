from app import create_app
import os

# create an app instance
app = create_app()

if __name__ == '__main__':
    # run the app
    app.run(host='0.0.0.0', port=os.environ.get('PORT') )